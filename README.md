This script is used in the GitLab pipeline of other projects.
In the project that need to use this build script, go to the Settings > CI / CD > General pipelines.
In the **Custom CI configuration path** enter gitlab-ci.yml@aurbuilds/gitlab-ci
